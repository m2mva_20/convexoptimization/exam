#!/bin/python

import sys
import warnings

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import tqdm

warnings.simplefilter("error")

from utils import (centeringObj, gradCentering, hessCentering, plotConvergence,
                   showSeparation, showSeparationListe)


def initProblem(m=10, m1=-3, v1=2, m2=3, v2=2):
    '''Initialize the problem with two 2D classes of data points according to two gaussians sample with moment (m_i, v_i)'''
    X = np.array([rd.normal(m1,v1, size=(m,2,1))] + [rd.normal(m2,v2, size=(m,2,1))]).reshape(2*m,2,1)
    Y = np.array([np.ones(m)] + [-np.ones(m)]).reshape(2*m,1)
    return X, Y

def centering_step(Y, X, C, tB, l0, eps, alpha = 0.25, beta = 0.7):
    '''Compute the centering step thanks to newton algorithm and a backtracking line search'''
    n   = X.shape[0]
    f   = lambda l : centeringObj(l, X, Y, C, tB)[0]
    df  = lambda l : gradCentering(l, X, Y, C, tB)
    ddf = lambda l : hessCentering(l, X, Y, C, tB)
    dec = 1
    x   = l0
    v   = [np.copy(x)]
    D = []
    while dec/2 > eps:
        cdf  = df(x)
        cddf = ddf(x)
        dx   = -np.linalg.inv(cddf).dot(cdf)
        dec  = cdf.T.dot(-dx)
        t    = 1
        while t>1e-12 and ( ((x+t*dx)/C-(x+t*dx)**2<0).any() or  f(x+t*dx) >= f(x)+alpha*t*cdf.T.dot(dx)):
            t = beta*t
        x  += t*dx
        dec = df(x).T.dot(np.linalg.inv(ddf(x)).dot(df(x)))
        v.append(np.copy(x))
        D.append(dec)
    return v, D

def barr_method(Y, X, C, l0, eps, mu=15):
    '''Compute the log barrier method to solve the dual separation problem'''
    t  = 1
    m  = 2*l0.shape[0]
    x, d  = centering_step(Y, X, C, t, l0, eps)
    V = []
    D = []
    for i in x:
        V.append(np.copy(i))
    for e in d:
        D.append(e[0,0])
    T = [len(x)]
    while m/t >= eps:
        t = mu*t
        x, d = centering_step(Y, X, C, t, x[-1], eps)
        for i in x:
            V.append(np.copy(i))
        T.append(len(x))
        for e in d:
            D.append(e[0,0])
    return V, T, D

def recoverPrimal(Y, X, l):
    w = np.zeros(X[0].shape)
    m = Y.shape[0]
    for i in range(m):
        w += l[i,0]*Y[i,0]*X[i,:]
    return -w

def main():
    X, Y = initProblem()
    C    = 0.1
    l    = np.ones(Y.shape)
    tB   = 1
    eps  = 1e-3
    V, T, D = barr_method(Y, X, C, l, eps)
    w    = recoverPrimal(Y, X, V[-1])
    showSeparation(X, Y, w)
    plotConvergence(X, Y, C, tB, V)

if __name__=="__main__":
    main()
