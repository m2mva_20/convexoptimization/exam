#!/bin/python

import matplotlib.pyplot as plt
import numpy as np


def centeringObj(l, X, Y, C, t):
    tmp = np.zeros(X[0].shape)
    m = Y.shape[0]
    for i in range(m):
        tmp += l[i,0]*Y[i,0]*X[i,:]
    return .5*t*tmp.T.dot(tmp)[0,0] - t*np.sum(l) - np.sum(np.log(l/C-l**2)),.5*tmp.T.dot(tmp)[0,0] - np.sum(l)

def gradCentering(l, X, Y, C, t):
    tmp = np.zeros(X[0].shape)
    m = Y.shape[0]
    for i in range(m):
        tmp += l[i,0]*Y[i,0]*X[i,:]
    g = np.zeros(l.shape)
    for i in range(m):
        g[i,0] = t*(Y[i,0]*X[i,:].T.dot(tmp)[0,0] -1) - (1-2*C*l[i,0])/(l[i,0]-C*l[i,0]**2)
    return g

def hessCentering(lamb, X, Y, C, t):
    m = lamb.shape[0]
    h = np.zeros((m,m))
    for k in range(m):
        for l in range(m):
            h[k,l] = t*Y[k,0]*Y[l,0]*X[k,:].T.dot(X[l,:])
            if k==l:
                h[k,l] += 2*(lamb[k,0]/C-lamb[k,0]**2 + (1/C-2*lamb[k,0])**2 )/(lamb[k,0]/C-lamb[k,0]**2)**2
    return h


def showSeparation(X, Y, w):
    plt.figure()
    plt.scatter(X[:,0,0], X[:,1,0], c=Y, alpha=.7, cmap='seismic')
    plt.plot([x for x in range(int(min(X[:,0, 0]))-1, int(max(X[:,0,0]))+1) ], [-w[0,0]/w[1,0]*x for x in range(int(min(X[:,0, 0]))-1, int(max(X[:,0,0]))+1) ], '--r', label='Separating hyperplane' )
    plt.legend()
    plt.grid()
    plt.savefig('../Fig/separation.pdf')
    plt.show()

def showSeparationListe(X, Y, W, Cliste):
    plt.figure()
    plt.scatter(X[:,0,0], X[:,1,0], c=Y, alpha=.7, cmap='seismic')
    i=0
    for w in W:
        plt.plot([x for x in range(int(min(X[:,0, 0]))-1, int(max(X[:,0,0]))+1) ], [-w[0,0]/w[1,0]*x for x in range(int(min(X[:,0, 0]))-1, int(max(X[:,0,0]))+1) ], '--', label='Separating hyperplane for C = ' + str(Cliste[i]) )
        i += 1
    plt.legend()
    plt.grid()
    plt.savefig('../Fig/separation.pdf')
    plt.show()

def plotConvergence(X, Y, C, t, V):
    f = lambda l : centeringObj(l, X, Y, C, t)[1]
    plt.figure()
    plt.plot(range(len(V)-1), list(map(lambda x : f(x)-f(V[-1]), V[:-1])))
    plt.grid()
    plt.yscale('log')
    plt.ylabel(r"$f(\lambda) - f(\lambda^*)$")
    plt.xlabel('#Newton iterations')
    plt.savefig('../Fig/convergence.pdf')
    plt.show()
